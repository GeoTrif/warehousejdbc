package org.example.warehousemanagementjdbc.util;

import java.util.Scanner;

/**
 * Utilitaty class used to enable user input.
 * 
 * @author GeoTrif
 *
 */
public class ScannerUtil {
	private Scanner input = new Scanner(System.in);

	public int integerScanner() {
		return input.nextInt();
	}

	public String stringScanner() {
		return input.next();
	}

	public double doubleScanner() {
		return input.nextDouble();
	}
}
