package org.example.warehousemanagementjdbc.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Utilitary class used to validate the password from the admin user.
 * 
 * @author GeoTrif
 *
 */
public class AdminPasswordValidator {

	/**
	 * Used to perform validation on the admin password.
	 * 
	 * @param password
	 *            which is the password to be tested.
	 * @return true if the password passes the validations.
	 */
	public boolean validateAdminPassword(String password) {
		if (checkIfAdminPasswordHasGoodSintax(password) && checkIfAdminPasswordInDataBase(password)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Used to perform syntax validation on the admin password.
	 * 
	 * @param password
	 *            which is the password to be tested.
	 * @return true if the password passes the validations.
	 */
	private boolean checkIfAdminPasswordHasGoodSintax(String password) {
		boolean isLetter = false;
		boolean isDigit = false;
		boolean isUpperCaseLetter = false;
		boolean isSymbol = false;
		boolean isValid = false;

		if (password.length() > 6 && password.length() < 20) {

			for (int i = 0; i < password.length(); i++) {

				if (Character.isAlphabetic(password.charAt(i)) == true) {
					isLetter = true;
				}

				if (Character.isDigit(password.charAt(i)) == true) {
					isDigit = true;
				}

				if (Character.isUpperCase(password.charAt(i)) == true) {
					isUpperCaseLetter = true;
				}

				if (!Character.isLetterOrDigit(password.charAt(i))) {
					isSymbol = true;
				}
			}

		}

		if (isLetter == true && isDigit == true && isUpperCaseLetter == true && isSymbol == true) {
			isValid = true;
		}

		return isValid;
	}

	/**
	 * Used to perform validation if the admin password matches the ones in the
	 * database.
	 * 
	 * @param password
	 *            which is the password to be tested.
	 * @return true if the password passes the validation.
	 */
	private boolean checkIfAdminPasswordInDataBase(String password) {
		boolean isInDataBase = false;
		String sql = "select * from admins";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String adminPassword = resultSet.getString("password");

				if (adminPassword.equals(password)) {
					isInDataBase = true;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return isInDataBase;
	}
}
