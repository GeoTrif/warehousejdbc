package org.example.warehousemanagementjdbc.util;

/**
 * Utilitary class that prints various menus.
 * 
 * @author GeoTrif
 *
 */
public class Menu {

	public void printMainMenu() {
		System.out.println("Warehouse Application");
		System.out.println("Enter:");
		System.out.println("\t1 - Login");
		System.out.println("\t2 - Create new account");
		System.out.println("\t3 - Product Catalogue");
		System.out.println("\t4 - Print main menu");
		System.out.println("\t5 - Exit application");
	}

	public void loginMenu() {
		System.out.println("Login Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Admin");
		System.out.println("\t2 - Customer");
		System.out.println("\t3 - Print Login Menu");
		System.out.println("\t4 - Back to Main Menu");
		System.out.println("\t5 - Exit application");
	}

	public void createNewAccountMenu() {
		System.out.println("New Account Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Admin account");
		System.out.println("\t2 - Customer account");
		System.out.println("\t3 - Print New Account Menu");
		System.out.println("\t4 - Back to Main Menu");
		System.out.println("\t5 - Exit application");
	}

	public void productCatalogueMenu() {
		System.out.println("Product Catalogue Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Products in stock");
		System.out.println("\t2 - Print Product Catalogue Menu");
		System.out.println("\t3 - Back to Main Menu");
		System.out.println("\t4 - Exit application");
	}

	public void adminUserMenu() {
		System.out.println("Admin User Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Admins manipulation");
		System.out.println("\t2 - Customers manipulation");
		System.out.println("\t3 - Orders manipulation");
		System.out.println("\t4 - Products manipulation");
		System.out.println("\t5 - Print general report");
		System.out.println("\t6 - Back to Main Menu");
		System.out.println("\t7 - Exit Application");
	}

	public void customerUserMenu() {
		System.out.println("Customer User Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Add order");
		System.out.println("\t2 - View product catalogue");
		System.out.println("\t3 - View orders");
		System.out.println("\t4 - Print order report");
		System.out.println("\t5 - Back to Main Menu");
		System.out.println("\t6 - Exit Application");
	}

	public void adminManipulationMenu() {
		System.out.println("Admins Manipulation Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Add admin");
		System.out.println("\t2 - Get admin by id");
		System.out.println("\t3 - Get all admins");
		System.out.println("\t4 - Update admin");
		System.out.println("\t5 - Delete admin");
		System.out.println("\t6 - Back to Admin Menu");
		System.out.println("\t7 - Exit Application");
	}

	public void customerManipulationMenu() {
		System.out.println("Customers Manipulation Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Add customer");
		System.out.println("\t2 - Get customer by id");
		System.out.println("\t3 - Get all customers");
		System.out.println("\t4 - Update customer");
		System.out.println("\t5 - Delete customer");
		System.out.println("\t6 - Back to Admin Menu");
		System.out.println("\t7 - Exit Application");
	}

	public void orderManipulationMenu() {
		System.out.println("Orders Manipulation Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Add order");
		System.out.println("\t2 - Get order by id");
		System.out.println("\t3 - Get all orders");
		System.out.println("\t4 - Update order");
		System.out.println("\t5 - Delete order");
		System.out.println("\t6 - Back to Admin Menu");
		System.out.println("\t7 - Exit Application");
	}

	public void productManipulationMenu() {
		System.out.println("Produts Manipulation Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - Add product");
		System.out.println("\t2 - Get product by id");
		System.out.println("\t3 - Get all products");
		System.out.println("\t4 - Update product");
		System.out.println("\t5 - Delete product");
		System.out.println("\t6 - Back to Admin Menu");
		System.out.println("\t7 - Exit Application");
	}

	public void reportingsMenu() {
		System.out.println("Reportings Menu");
		System.out.println("Enter:");
		System.out.println("\t1 - CSV Excel Report");
		System.out.println("\t2 - Text File Report");
		System.out.println("\t3 - PDF Document Report");
		System.out.println("\t4 - Back to Admin Menu");
		System.out.println("\t5 - Exit Application");
	}
}
