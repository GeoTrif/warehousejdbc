package org.example.warehousemanagementjdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.warehousemanagementjdbc.model.Product;

/**
 * Mapper class used to get the products data from the database as a
 * collection(list).
 * 
 * @author GeoTrif
 *
 */
public class ProductMapper {

	public List<Product> mapResultSetToProduct(ResultSet resultSet) {
		List<Product> products = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Product product = new Product();
				product.setProductId(resultSet.getInt("product_id"));
				product.setProductName(resultSet.getString("product_name"));
				product.setProductPrice(resultSet.getDouble("product_price"));
				products.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

}
