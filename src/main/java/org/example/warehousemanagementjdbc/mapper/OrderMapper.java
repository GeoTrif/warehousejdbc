package org.example.warehousemanagementjdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.warehousemanagementjdbc.model.Order;

/**
 * Mapper class used to get the orders data from the database as a
 * collection(list).
 * 
 * @author GeoTrif
 *
 */
public class OrderMapper {

	public List<Order> mapResultSetToOrder(ResultSet resultSet) {
		List<Order> orders = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Order order = new Order();
				order.setOrderId(resultSet.getInt("order_id"));
				order.setOrderDate(resultSet.getDate("order_date"));
				order.setDeliveryDate(resultSet.getDate("delivery_date"));
				order.setCustomerId(resultSet.getInt("customer_id"));
				orders.add(order);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return orders;
	}

}
