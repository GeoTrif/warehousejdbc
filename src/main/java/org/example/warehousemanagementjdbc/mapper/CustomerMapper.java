package org.example.warehousemanagementjdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.example.warehousemanagementjdbc.model.Customer;

/**
 * Mapper class used to get the customers data from the database as a
 * collection(list).
 * 
 * @author GeoTrif
 *
 */
public class CustomerMapper {

	public List<Customer> mapResultSetToCustomer(ResultSet resultSet) {
		List<Customer> customers = new ArrayList<>();

		try {
			while (resultSet.next()) {
				Customer customer = new Customer();
				customer.setCustomerId(resultSet.getInt("customer_id"));
				customer.setFirstName(resultSet.getString("customer_first_name"));
				customer.setLastName(resultSet.getString("customer_last_name"));
				customer.setUserName(resultSet.getString("customer_user_name"));
				customer.setPassword(resultSet.getString("customer_password"));
				customer.setEmail(resultSet.getString("customer_email"));
				customer.setPhoneNumber(resultSet.getString("customer_phone_number"));
				customers.add(customer);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}
}
