package org.example.warehousemanagementjdbc.controller;

import java.io.IOException;

import org.example.warehousemanagementjdbc.reporting.AdminCSVExcelGeneralReport;
import org.example.warehousemanagementjdbc.reporting.AdminFileGeneralReport;
import org.example.warehousemanagementjdbc.reporting.AdminPDFGeneralReport;
import org.example.warehousemanagementjdbc.service.AdminService;
import org.example.warehousemanagementjdbc.service.CustomerService;
import org.example.warehousemanagementjdbc.service.OrderService;
import org.example.warehousemanagementjdbc.service.ProductService;
import org.example.warehousemanagementjdbc.util.Menu;
import org.example.warehousemanagementjdbc.util.ScannerUtil;

import jxl.write.WriteException;

/**
 * Controller class used to make operations when valid admin user logs in.
 * 
 * @author GeoTrif
 * 
 */
public class AdminController {
	private static Menu menu = new Menu();
	private static ScannerUtil scanner = new ScannerUtil();
	private static boolean flag = false;
	private static WarehouseController warehouseController = new WarehouseController();
	private static AdminService adminService = new AdminService();
	private static CustomerService customerService = new CustomerService();
	private static OrderService orderService = new OrderService();
	private static ProductService productService = new ProductService();
	private static AdminCSVExcelGeneralReport csvExcelReport = new AdminCSVExcelGeneralReport();
	private static AdminFileGeneralReport fileReport = new AdminFileGeneralReport();
	private static AdminPDFGeneralReport pdfReport = new AdminPDFGeneralReport();

	/**
	 * Used to access the primary operations of the admin user.
	 * 
	 * @throws IOException
	 */
	public static void adminUserFunctionality() throws IOException {
		menu.adminUserMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				adminManipulationFunctionality();
				break;

			case 2:
				customerManipulationFunctionality();
				break;

			case 3:
				orderManipulationFunctionalty();
				break;

			case 4:
				productManipulationFunctionality();
				break;

			case 5:
				reportFunctionality();
				break;

			case 6:
				warehouseController.mainMenuFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please select a valid choice.");
				adminUserFunctionality();
			}
		}
	}

	/**
	 * Used to add,search,update or delete on the admin table from the MySQL
	 * Database.
	 * 
	 * @throws IOException
	 */
	private static void adminManipulationFunctionality() throws IOException {
		menu.adminManipulationMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				adminService.addAdminService();
				adminManipulationFunctionality();
				break;

			case 2:
				adminService.getAdminByIdService();
				adminManipulationFunctionality();
				break;

			case 3:
				adminService.getAllAdminsService();
				adminManipulationFunctionality();
				break;

			case 4:
				adminService.updateAdminService();
				adminManipulationFunctionality();
				break;

			case 5:
				adminService.deleteAdminService();
				adminManipulationFunctionality();
				break;

			case 6:
				adminUserFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please select a valid choice.");
				adminManipulationFunctionality();
			}
		}
	}

	/**
	 * Used to add,search,update or delete on the customers table from the MySQL
	 * Database.
	 * 
	 * @throws IOException
	 */
	private static void customerManipulationFunctionality() throws IOException {
		menu.customerManipulationMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				customerService.addCustomerService();
				customerManipulationFunctionality();
				break;

			case 2:
				customerService.getCustomerByIdService();
				customerManipulationFunctionality();
				break;

			case 3:
				customerService.getAllCustomersService();
				customerManipulationFunctionality();
				break;

			case 4:
				customerService.updateCustomerService();
				customerManipulationFunctionality();
				break;

			case 5:
				customerService.deleteCustomerService();
				customerManipulationFunctionality();
				break;

			case 6:
				adminUserFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please select a valid choice.");
				adminManipulationFunctionality();
			}
		}
	}

	/**
	 * Used to add,search,update or delete on the orders table from the MySQL
	 * Database.
	 * 
	 * @throws IOException
	 */
	private static void orderManipulationFunctionalty() throws IOException {
		menu.orderManipulationMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				orderService.addAOrderService();
				orderManipulationFunctionalty();
				break;

			case 2:
				orderService.getOrderByIdService();
				orderManipulationFunctionalty();
				break;

			case 3:
				orderService.getAllOrdersService();
				orderManipulationFunctionalty();
				break;

			case 4:
				orderService.updateOrderService();
				orderManipulationFunctionalty();
				break;

			case 5:
				orderService.deleteOrderService();
				orderManipulationFunctionalty();
				break;

			case 6:
				adminUserFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please select a valid choice.");
				adminManipulationFunctionality();
			}
		}
	}

	/**
	 * Used to add,search,update or delete on the products table from the MySQL
	 * Database.
	 * 
	 * @throws IOException
	 */
	private static void productManipulationFunctionality() throws IOException {
		menu.productManipulationMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				productService.addAProductService();
				productManipulationFunctionality();
				break;

			case 2:
				productService.getProductByIdService();
				productManipulationFunctionality();
				break;

			case 3:
				productService.getAllProductsService();
				productManipulationFunctionality();
				break;

			case 4:
				productService.updateProductService();
				productManipulationFunctionality();
				break;

			case 5:
				productService.deleteProductService();
				productManipulationFunctionality();
				break;

			case 6:
				adminUserFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please select a valid choice.");
				adminManipulationFunctionality();
			}
		}
	}

	/**
	 * Used to create reports in different formats.
	 * 
	 * @throws IOException
	 */
	private static void reportFunctionality() throws IOException {
		menu.reportingsMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				try {
					csvExcelReport.makeCSV();
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				reportFunctionality();
				break;

			case 2:
				try {
					fileReport.makeFileReport();
				} catch (IOException e) {
					e.printStackTrace();
				}
				reportFunctionality();
				break;

			case 3:
				pdfReport.makePDFReport();
				reportFunctionality();
				break;

			case 4:
				adminUserFunctionality();
				break;

			case 5:
				System.out.println("Closing Application...");
				flag = true;
				break;

			default:
				System.out.println("Enter a valid choice.");
				reportFunctionality();
			}
		}
	}
}
