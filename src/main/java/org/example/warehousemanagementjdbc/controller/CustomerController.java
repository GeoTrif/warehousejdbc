package org.example.warehousemanagementjdbc.controller;

import java.io.IOException;

import org.example.warehousemanagementjdbc.reporting.CustomerCSVExcelOrdersDocument;
import org.example.warehousemanagementjdbc.reporting.CustomerFileOrdersDocument;
import org.example.warehousemanagementjdbc.reporting.CustomerPDFOrdersDocument;
import org.example.warehousemanagementjdbc.service.OrderService;
import org.example.warehousemanagementjdbc.service.ProductService;
import org.example.warehousemanagementjdbc.util.Menu;
import org.example.warehousemanagementjdbc.util.ScannerUtil;

import jxl.write.WriteException;

/**
 * Controller class used to make operations when valid customer user logs in.
 * 
 * @author GeoTrif
 * 
 */
public class CustomerController {
	private static Menu menu = new Menu();
	private static ScannerUtil scanner = new ScannerUtil();
	private static boolean flag = false;
	private static WarehouseController warehouseController = new WarehouseController();
	private static OrderService orderService = new OrderService();
	private static ProductService productService = new ProductService();
	private static CustomerCSVExcelOrdersDocument csvExcelReport = new CustomerCSVExcelOrdersDocument();
	private static CustomerFileOrdersDocument fileReport = new CustomerFileOrdersDocument();
	private static CustomerPDFOrdersDocument pdfReport = new CustomerPDFOrdersDocument();

	/**
	 * Used to access the primary operations of the customer user.
	 * 
	 * @throws IOException
	 */
	public static void customerMenuFunctionality(String userName) throws IOException {
		menu.customerUserMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				orderService.addAOrderService();
				customerMenuFunctionality(userName);
				break;

			case 2:
				productService.getAllProductsService();
				customerMenuFunctionality(userName);
				break;

			case 3:
				orderService.getOrderWithProductsService(userName);
				System.out.println("-----------------------");
				productService.getProductsByOrderIdService(userName);
				customerMenuFunctionality(userName);
				break;

			case 4:
				reportFunctionality(userName);
				break;

			case 5:
				customerMenuFunctionality(userName);
				break;

			case 6:
				warehouseController.mainMenuFunctionality();
				break;

			case 7:
				System.out.println("Closing application...");
				flag = true;
				break;

			default:
				System.out.println("Please enter a valid choice.");

			}
		}
	}

	private static void reportFunctionality(String userName) throws IOException {
		menu.reportingsMenu();
		System.out.println("Enter your choice:");
		int choice = scanner.integerScanner();

		while (flag != true) {

			switch (choice) {
			case 1:
				try {
					csvExcelReport.makeCSV(userName);
				} catch (WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				reportFunctionality(userName);
				break;

			case 2:
				try {
					fileReport.makeFileReport(userName);
				} catch (IOException e) {
					e.printStackTrace();
				}
				reportFunctionality(userName);
				break;

			case 3:
				pdfReport.makePDFReport(userName);
				reportFunctionality(userName);
				break;

			case 4:
				customerMenuFunctionality(userName);
				break;

			case 5:
				System.out.println("Closing Application...");
				flag = true;
				break;

			default:
				System.out.println("Enter a valid choice.");
				reportFunctionality(userName);
			}
		}
	}
}
