package org.example.warehousemanagementjdbc.model;

import java.util.Date;

/**
 * Model class used to make new orders.
 * 
 * @author GeoTrif
 *
 */

public class Order {
	private int orderId;
	private String orderName;
	private Date orderDate;
	private Date deliveryDate;
	private int customerId;

	public Order() {
	}

	public Order(int orderId) {
		this.orderId = orderId;
	}

	public Order(String orderName, Date orderDate, Date deliveryDate, int customerId) {
		this.orderName = orderName;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.customerId = customerId;
	}

	public Order(int orderId, String orderName, Date orderDate, Date deliveryDate, int customerId) {
		this.orderId = orderId;
		this.orderName = orderName;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.customerId = customerId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + orderId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (orderId != other.orderId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", orderName=" + orderName + ", orderDate=" + orderDate + ", deliveryDate="
				+ deliveryDate + ", customerId=" + customerId + "]";
	}
}
