package org.example.warehousemanagementjdbc.reporting;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.warehousemanagementjdbc.util.ConnectionUtil;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * Utility class used to create a SCVExcel General Reporting that includes all
 * the data from the database.Only for admin users.
 * 
 * @author GeoTrif
 *
 */
public class AdminCSVExcelGeneralReport {

	/**
	 * Used to create the file on the hardDisk and to write the data in the file.
	 * 
	 * @throws IOException
	 * @throws WriteException
	 */
	public void makeCSV() throws IOException, WriteException {
		File f = new File(
				"C:\\Users\\GeoTrif\\Desktop\\OOP Projects\\WarehouseManagement\\Reportings\\CSVExcelReporting.xlss");
		WritableWorkbook myExcel = Workbook.createWorkbook(f);
		WritableSheet mySheet = myExcel.createSheet("Order Report", 0);

		Label titleLabel = new Label(0, 0, "General Report");
		Label adminsLabel = new Label(0, 1, "Admins Details");
		Label customersLabel = new Label(1, 1, "Customers Details");
		Label ordersLabel = new Label(2, 1, "Orders Details");
		Label productsLabel = new Label(3, 1, "Products Details");

		mySheet.addCell(titleLabel);
		mySheet.addCell(adminsLabel);
		mySheet.addCell(customersLabel);
		mySheet.addCell(ordersLabel);
		mySheet.addCell(productsLabel);

		getAdminsDetails(mySheet);
		getCustomersDetails(mySheet);
		getOrdersDetails(mySheet);
		getProductsDetails(mySheet);
		myExcel.write();
		myExcel.close();
		System.out.println("Excel created.");
	}

	/**
	 * Used to get the admins data from the database.
	 * 
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void getAdminsDetails(WritableSheet mySheet) throws IOException, RowsExceededException, WriteException {
		int row = 3;
		int col = 0;

		Label idLabel = null;
		Label firstNameLabel = null;
		Label lastNameLabel = null;
		Label userNameLabel = null;
		Label passwordLabel = null;
		Label emailLabel = null;
		Label phoneNumberLabel = null;
		Label emptyCellLabel = null;

		String sql = "select * from admins";
		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("admin_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String userName = resultSet.getString("user_name");
				String password = resultSet.getString("password");
				String email = resultSet.getString("email");
				String phoneNumber = resultSet.getString("phone_number");

				idLabel = new Label(col, row, "Admin Id: " + Integer.toString(id));
				firstNameLabel = new Label(col, row + 1, "Admin first name: " + firstName);
				lastNameLabel = new Label(col, row + 2, "Admin last name: " + lastName);
				userNameLabel = new Label(col, row + 3, "Admin username: " + userName);
				passwordLabel = new Label(col, row + 4, "Admin password: " + password);
				emailLabel = new Label(col, row + 5, "Admin email: " + email);
				phoneNumberLabel = new Label(col, row + 6, "Admin phone number: " + phoneNumber);
				emptyCellLabel = new Label(col, row + 7, " ");
				row += 8;

				mySheet.addCell(idLabel);
				mySheet.addCell(firstNameLabel);
				mySheet.addCell(lastNameLabel);
				mySheet.addCell(userNameLabel);
				mySheet.addCell(passwordLabel);
				mySheet.addCell(emailLabel);
				mySheet.addCell(phoneNumberLabel);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the customers data from the database.
	 * 
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void getCustomersDetails(WritableSheet mySheet) throws RowsExceededException, WriteException {
		int row = 3;
		int col = 1;

		Label idLabel = null;
		Label firstNameLabel = null;
		Label lastNameLabel = null;
		Label userNameLabel = null;
		Label passwordLabel = null;
		Label emailLabel = null;
		Label phoneNumberLabel = null;
		Label emptyCellLabel = null;

		String sql = "select * from customers";
		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("customer_first_name");
				String lastName = resultSet.getString("customer_last_name");
				String userName = resultSet.getString("customer_user_name");
				String password = resultSet.getString("customer_password");
				String email = resultSet.getString("customer_email");
				String phoneNumber = resultSet.getString("customer_phone_number");

				idLabel = new Label(col, row, "Customer Id: " + Integer.toString(id));
				firstNameLabel = new Label(col, row + 1, "Customer first name: " + firstName);
				lastNameLabel = new Label(col, row + 2, "Customer last name: " + lastName);
				userNameLabel = new Label(col, row + 3, "Customer username: " + userName);
				passwordLabel = new Label(col, row + 4, "Customer password: " + password);
				emailLabel = new Label(col, row + 5, "Customer email: " + email);
				phoneNumberLabel = new Label(col, row + 6, "Customer phone number: " + phoneNumber);
				emptyCellLabel = new Label(col, row + 7, " ");
				row += 8;

				mySheet.addCell(idLabel);
				mySheet.addCell(firstNameLabel);
				mySheet.addCell(lastNameLabel);
				mySheet.addCell(userNameLabel);
				mySheet.addCell(passwordLabel);
				mySheet.addCell(emailLabel);
				mySheet.addCell(phoneNumberLabel);

			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Used to get the orders data from the database.
	 * 
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void getOrdersDetails(WritableSheet mySheet) {
		int row = 3;
		int col = 2;

		Label idLabel = null;
		Label orderNameLabel = null;
		Label orderDateLAbel = null;
		Label deliveryDateLAbel = null;
		Label customerIdLabel = null;
		Label emptyCellLabel = null;

		String sql = "select * from orders";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customerId = resultSet.getInt("customer_id");

				idLabel = new Label(col, row, "Order Id: " + Integer.toString(id));
				orderNameLabel = new Label(col, row + 1, "Order name: " + orderName);
				orderDateLAbel = new Label(col, row + 2, "Order date: " + orderDate);
				deliveryDateLAbel = new Label(col, row + 3, "Delivery date: " + deliveryDate);
				customerIdLabel = new Label(col, row + 4, "Customer Id: " + customerId);
				emptyCellLabel = new Label(col, row + 5, " ");
				row += 6;
			}

			statement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Used to get the products data from the database.
	 * 
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void getProductsDetails(WritableSheet mySheet) throws IOException, RowsExceededException, WriteException {
		int row = 3;
		int col = 3;
		Label idLabel = null;
		Label nameLabel = null;
		Label priceLabel = null;
		Label emptyCellLabel = null;

		Label intro = new Label(0, 0, "Product details");

		String sql = "select * from products";

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				idLabel = new Label(col, row, "Product Id: " + Integer.toString(id));
				nameLabel = new Label(col, row + 1, "Product Name: " + productName);
				priceLabel = new Label(col, row + 2, "Product Price: " + productPrice);
				emptyCellLabel = new Label(col, row + 3, " ");
				row += 4;

				mySheet.addCell(idLabel);
				mySheet.addCell(nameLabel);
				mySheet.addCell(priceLabel);
			}

			statement.close();
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
