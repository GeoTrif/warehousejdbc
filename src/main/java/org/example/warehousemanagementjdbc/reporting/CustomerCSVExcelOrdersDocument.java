package org.example.warehousemanagementjdbc.reporting;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.example.warehousemanagementjdbc.util.ConnectionUtil;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

/**
 * Utility class used to create a SCVExcel Orders Reporting that includes orders
 * and products data from the database for a particular customer.Only for
 * customer users.
 * 
 * @author GeoTrif
 *
 */
public class CustomerCSVExcelOrdersDocument {

	/**
	 * Used to create the file on the hardDisk and to write the data in the file.
	 * 
	 * @throws IOException
	 * @throws WriteException
	 */
	public void makeCSV(String customerUsername) throws IOException, WriteException {
		File f = new File(
				"C:\\Users\\GeoTrif\\Desktop\\OOP Projects\\WarehouseManagement\\Reportings\\CSVExcelReporting.xlss");
		WritableWorkbook myExcel = Workbook.createWorkbook(f);
		WritableSheet mySheet = myExcel.createSheet("Order Report", 0);

		Label titleLabel = new Label(0, 0, "Customer Order Report");
		Label adminsLabel = new Label(0, 1, "Order Details");
		Label customersLabel = new Label(1, 1, "Product Details");

		mySheet.addCell(titleLabel);
		mySheet.addCell(adminsLabel);
		mySheet.addCell(customersLabel);
		addCustomerOrderDetails(customerUsername, mySheet);
		addCustomerProductsByOrderDetails(customerUsername, mySheet);

		myExcel.write();
		myExcel.close();
		System.out.println("Excel created.");

	}

	/**
	 * Used to get the orders data from the database for a particular customer.
	 * 
	 * @param customerUsername
	 *            which is the username used to login.
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void addCustomerOrderDetails(String customerUsername, WritableSheet mySheet)
			throws RowsExceededException, WriteException {
		int row = 3;
		int col = 0;

		Label orderNameLabel = null;
		Label orderDateLabel = null;
		Label deliveryDateLabel = null;
		Label productIdLabel = null;
		Label emptyCellLabel = null;

		String sql = "SELECT order_name,order_date,delivery_date,orders_products_products_id FROM warehouse.orders inner join "
				+ "orders_products on order_id = orders_products_orders_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int productId = resultSet.getInt("orders_products_products_id");

				orderNameLabel = new Label(col, row + 1, orderName);
				orderDateLabel = new Label(col, row + 2, orderDate.toString());
				deliveryDateLabel = new Label(col, row + 3, deliveryDate.toString());
				productIdLabel = new Label(col, row + 4, Integer.toString(productId));
				emptyCellLabel = new Label(col, row + 5, "");
				row += 6;

				mySheet.addCell(orderNameLabel);
				mySheet.addCell(orderDateLabel);
				mySheet.addCell(deliveryDateLabel);
				mySheet.addCell(productIdLabel);
				mySheet.addCell(emptyCellLabel);
			}

			preparedStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get the products data from the database for a particular customer.
	 * 
	 * @param customerUsername
	 *            which is the username used to login.
	 * @param mySheet
	 *            which will be the excel sheet of our SCV.
	 * @throws IOException
	 * @throws RowsExceededException
	 * @throws WriteException
	 */
	private void addCustomerProductsByOrderDetails(String customerUsername, WritableSheet mySheet) {
		int row = 3;
		int col = 1;

		Label productNameLabel = null;
		Label productPriceLabel = null;
		Label orderIdLabel = null;
		Label totalPriceLabel = null;
		Label emptyCellLabel = null;

		String sql = "SELECT product_name,product_price,orders_products_orders_id FROM warehouse.products inner join "
				+ "orders_products on product_id = orders_products_products_id inner join "
				+ "orders on orders_products_orders_id = orders.order_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();
			double totalPrice = 0;

			while (resultSet.next()) {
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");
				int id = resultSet.getInt("orders_products_orders_id");
				totalPrice += productPrice;

				productNameLabel = new Label(col, row + 1, productName);
				productPriceLabel = new Label(col, row + 2, Double.toString(productPrice));
				orderIdLabel = new Label(col, row + 3, Integer.toString(id));
				emptyCellLabel = new Label(col, row + 4, "");
				row += 6;

			}

			totalPriceLabel = new Label(col + 1, row, Double.toString(totalPrice));

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
