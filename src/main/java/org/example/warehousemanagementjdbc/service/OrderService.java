package org.example.warehousemanagementjdbc.service;

import java.util.Date;

import org.example.warehousemanagementjdbc.dao.OrderDaoImpl;
import org.example.warehousemanagementjdbc.model.Order;
import org.example.warehousemanagementjdbc.util.ScannerUtil;

/**
 * Service class used to interact with the user input in order to do operations
 * on the orders table from the data base.
 * 
 * @author GeoTrif
 *
 */
public class OrderService {
	private ScannerUtil scanner = new ScannerUtil();
	private OrderDaoImpl orderDaoImpl = new OrderDaoImpl();

	public void addAOrderService() {
		System.out.println("Enter order name:");
		String orderName = scanner.stringScanner();
		System.out.println("Enter order date:");
		Date orderDate = new Date();
		System.out.println("Enter delivery date:");
		Date deliveryDate = new Date();
		System.out.println("Enter customer id");
		int customerId = scanner.integerScanner();

		Order order = new Order(orderName, orderDate, deliveryDate, customerId);
		orderDaoImpl.addOrder(order);
	}

	public void getOrderByIdService() {
		System.out.println("Enter order id:");
		int id = scanner.integerScanner();
		orderDaoImpl.getOrderById(id);
	}

	public void getAllOrdersService() {
		orderDaoImpl.getAllOrders();
	}

	public void updateOrderService() {
		System.out.println("Enter order id:");
		int orderId = scanner.integerScanner();
		System.out.println("Enter order name:");
		String orderName = scanner.stringScanner();
		System.out.println("Enter order date:");
		Date orderDate = new Date();
		System.out.println("Enter delivery date:");
		Date deliveryDate = new Date();
		System.out.println("Enter customer id");
		int customerId = scanner.integerScanner();

		Order order = new Order(orderId, orderName, orderDate, deliveryDate, customerId);
		orderDaoImpl.updateOrder(order);
	}

	public void deleteOrderService() {
		System.out.println("Enter order id:");
		int id = scanner.integerScanner();

		Order order = new Order(id);
		orderDaoImpl.deleteOrder(order);
	}

	public void getOrderWithProductsService(String customerUsername) {
		orderDaoImpl.getOrderWithProducts(customerUsername);
	}
}
