package org.example.warehousemanagementjdbc.dao;

import java.util.List;

import org.example.warehousemanagementjdbc.model.Product;

/**
 * Interface used for product data base operations implementation.
 * 
 * @author GeoTrif
 *
 */
public interface ProductDao {

	public void addProduct(Product product);

	public Product getProductById(int productId);

	public List<Product> getAllProducts();

	public void updateProduct(Product product);

	public void deleteProduct(Product product);

	public void getProductsByOrderId(String customerUsername);

}
