package org.example.warehousemanagementjdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.warehousemanagementjdbc.mapper.ProductMapper;
import org.example.warehousemanagementjdbc.model.Product;
import org.example.warehousemanagementjdbc.util.ConnectionUtil;

/**
 * Operational class used to make operations on the products table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class ProductDaoImpl implements ProductDao {
	private ProductMapper productMapper = new ProductMapper();

	/**
	 * Used to add products to the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void addProduct(Product product) {
		String sql = "insert into products(product_name,product_price) values (?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, product.getProductName());
			preparedStatement.setDouble(2, product.getProductPrice());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get products by product id from the products table in the warehouse
	 * dataBase.
	 * 
	 * @param productId
	 *            which is an integer.
	 */
	public Product getProductById(int productId) {
		String sql = "select * from products where product_id = ?";
		Product product = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, productId);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				System.out.println("Product id: " + id);
				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);
			}

			preparedStatement.close();
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return product;
	}

	/**
	 * Used to get all products from the products table in the warehouse dataBase.
	 * 
	 */
	public List<Product> getAllProducts() {
		String sql = "select * from products";
		List<Product> products = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("product_id");
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");

				System.out.println("Product id: " + id);
				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);
			}

			products = productMapper.mapResultSetToProduct(resultSet);

			statement.close();
			resultSet.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return products;
	}

	/**
	 * Used to update proucts from the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void updateProduct(Product product) {
		String sql = "update products set product_name = ?,product_price = ? where product_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, product.getProductName());
			preparedStatement.setDouble(2, product.getProductPrice());
			preparedStatement.setInt(3, product.getProductId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to delete products from the products table in the warehouse dataBase.
	 * 
	 * @param product
	 *            which is an object modeled by Product class.
	 */
	public void deleteProduct(Product product) {
		String sql = "delete from products where product_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, product.getProductId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get all the products from an order by using the orders id.
	 * 
	 * @param customerUsername
	 *            which is the validated username that will do the query filtering.
	 */
	public void getProductsByOrderId(String customerUsername) {
		String sql = "SELECT product_name,product_price,orders_products_orders_id FROM warehouse.products inner join "
				+ "orders_products on product_id = orders_products_products_id inner join "
				+ "orders on orders_products_orders_id = orders.order_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();
			double totalPrice = 0;

			while (resultSet.next()) {
				String productName = resultSet.getString("product_name");
				double productPrice = resultSet.getDouble("product_price");
				int id = resultSet.getInt("orders_products_orders_id");
				totalPrice += productPrice;

				System.out.println("Product name: " + productName);
				System.out.println("Product price: " + productPrice);
				System.out.println("Order id: " + id);
			}

			System.out.println("Total price: " + totalPrice);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
