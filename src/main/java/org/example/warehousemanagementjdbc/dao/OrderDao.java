package org.example.warehousemanagementjdbc.dao;

import java.util.List;

import org.example.warehousemanagementjdbc.model.Order;

/**
 * Interface used for order data base operations implementation.
 * 
 * @author GeoTrif
 *
 */
public interface OrderDao {

	public void addOrder(Order order);

	public Order getOrderById(int orderId);

	public List<Order> getAllOrders();

	public void updateOrder(Order order);

	public void deleteOrder(Order order);

	public void getOrderWithProducts(String customerUsername);
}
