package org.example.warehousemanagementjdbc.dao;

import java.util.List;

import org.example.warehousemanagementjdbc.model.Customer;

/**
 * Interface used for customer data base operations implementation.
 * 
 * @author GeoTrif
 *
 */
public interface CustomerDao {

	public void addCustomer(Customer customer);

	public Customer getCustomerById(int customerId);

	public List<Customer> getAllCustomers();

	public void updateCustomer(Customer customer);

	public void deleteCustomer(Customer customer);
}
