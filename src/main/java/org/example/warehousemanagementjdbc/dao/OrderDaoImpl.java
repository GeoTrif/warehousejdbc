package org.example.warehousemanagementjdbc.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.warehousemanagementjdbc.mapper.OrderMapper;
import org.example.warehousemanagementjdbc.model.Order;
import org.example.warehousemanagementjdbc.util.ConnectionUtil;

/**
 * Operational class used to make operations on the orders table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class OrderDaoImpl implements OrderDao {
	private OrderMapper orderMapper = new OrderMapper();

	/**
	 * Used to add orders to the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void addOrder(Order order) {
		String sql = "insert into orders(order_name,order_date,delivery_date,customer_id) values (?,?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, order.getOrderName());
			preparedStatement.setDate(2, (Date) order.getOrderDate());
			preparedStatement.setDate(3, (Date) order.getDeliveryDate());
			preparedStatement.setInt(4, order.getCustomerId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get orders by order id from the orders table in the warehouse
	 * dataBase.
	 * 
	 * @param orderId
	 *            which is an integer.
	 */
	public Order getOrderById(int orderId) {
		String sql = "select * from orders where order_id = ?";
		Order order = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, orderId);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customerId = resultSet.getInt("customer_id");

				System.out.println("Order id: " + id);
				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Customer id: " + customerId);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return order;

	}

	/**
	 * Used to get all orders from the orders table in the warehouse dataBase.
	 * 
	 */
	public List<Order> getAllOrders() {
		String sql = "select * from orders";
		List<Order> orders = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("order_id");
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int customerId = resultSet.getInt("customer_id");

				System.out.println("Order id: " + id);
				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Customer id: " + customerId);
			}

			orders = orderMapper.mapResultSetToOrder(resultSet);

			statement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return orders;
	}

	/**
	 * Used to update orders from the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void updateOrder(Order order) {
		String sql = "update orders set order_name = ?,order_date = ?,delivery_date = ?,customer_id = ? where order_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, order.getOrderName());
			preparedStatement.setDate(2, (Date) order.getOrderDate());
			preparedStatement.setDate(3, (Date) order.getDeliveryDate());
			preparedStatement.setInt(4, order.getCustomerId());
			preparedStatement.setInt(5, order.getOrderId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to delete orders from the orders table in the warehouse dataBase.
	 * 
	 * @param order
	 *            which is an object modeled by Order class.
	 */
	public void deleteOrder(Order order) {
		String sql = "delete from orders where order_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, order.getOrderId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get a particular order containing the products ordered by a user.
	 * 
	 * @param customerUsername
	 *            which is the validated username that will do the query filtering.
	 */
	public void getOrderWithProducts(String customerUsername) {
		String sql = "SELECT order_name,order_date,delivery_date,orders_products_products_id FROM warehouse.orders inner join "
				+ "orders_products on order_id = orders_products_orders_id inner join "
				+ "customers on orders.customer_id = customers.customer_id where customer_user_name = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customerUsername);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String orderName = resultSet.getString("order_name");
				Date orderDate = resultSet.getDate("order_date");
				Date deliveryDate = resultSet.getDate("delivery_date");
				int productId = resultSet.getInt("orders_products_products_id");

				System.out.println("Order name: " + orderName);
				System.out.println("Order date: " + orderDate);
				System.out.println("Delivery date: " + deliveryDate);
				System.out.println("Product id: " + productId);
			}

			preparedStatement.close();
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
