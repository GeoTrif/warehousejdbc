package org.example.warehousemanagementjdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.warehousemanagementjdbc.mapper.CustomerMapper;
import org.example.warehousemanagementjdbc.model.Customer;
import org.example.warehousemanagementjdbc.util.ConnectionUtil;

/**
 * Operational class used to make operations on the customers table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class CustomerDaoImpl implements CustomerDao {
	private CustomerMapper customerMapper = new CustomerMapper();

	/**
	 * Used to add customers users to the customers table in the warehouse dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void addCustomer(Customer customer) {
		String sql = "insert into customers(customer_first_name,customer_last_name,customer_user_name,customer_password,customer_email,customer_phone_number) values(?,?,?,?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customer.getFirstName());
			preparedStatement.setString(2, customer.getLastName());
			preparedStatement.setString(3, customer.getUserName());
			preparedStatement.setString(4, customer.getPassword());
			preparedStatement.setString(5, customer.getEmail());
			preparedStatement.setString(6, customer.getPhoneNumber());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get customer users by customer id from the customers table in the
	 * warehouse dataBase.
	 * 
	 * @param customerId
	 *            which is an integer.
	 */
	public Customer getCustomerById(int customerId) {
		String sql = "select * from customers where customer_id = ?";
		Customer customer = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, customerId);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("customer_first_name");
				String lastName = resultSet.getString("customer_last_name");
				String userName = resultSet.getString("customer_user_name");
				String password = resultSet.getString("customer_password");
				String email = resultSet.getString("customer_email");
				String phoneNumber = resultSet.getString("customer_phone_number");

				System.out.println("Customer id: " + id);
				System.out.println("Customer first name: " + firstName);
				System.out.println("Customer last name: " + lastName);
				System.out.println("Customer username: " + userName);
				System.out.println("Customer password: " + password);
				System.out.println("Customer email: " + email);
				System.out.println("Customer phone number: " + phoneNumber);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customer;
	}

	/**
	 * Used to get all customers users from the customers table in the warehouse
	 * dataBase.
	 * 
	 */
	public List<Customer> getAllCustomers() {
		String sql = "select * from customers";
		List<Customer> customers = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("customer_id");
				String firstName = resultSet.getString("customer_first_name");
				String lastName = resultSet.getString("customer_last_name");
				String userName = resultSet.getString("customer_user_name");
				String password = resultSet.getString("customer_password");
				String email = resultSet.getString("customer_email");
				String phoneNumber = resultSet.getString("customer_phone_number");

				System.out.println("Customer id:" + id);
				System.out.println("Customer first name: " + firstName);
				System.out.println("Customer last name: " + lastName);
				System.out.println("Customer username: " + userName);
				System.out.println("Customer password: " + password);
				System.out.println("Customer email: " + email);
				System.out.println("Customer phone number: " + phoneNumber);
			}

			customers = customerMapper.mapResultSetToCustomer(resultSet);

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return customers;
	}

	/**
	 * Used to update customers users from the customers table in the warehouse
	 * dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void updateCustomer(Customer customer) {
		String sql = "update customers set customer_first_name = ?,customer_last_name = ?,customer_user_name = ?,customer_password = ?,customer_email = ?,cutomer_phone_number = ? where customer_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, customer.getFirstName());
			preparedStatement.setString(2, customer.getLastName());
			preparedStatement.setString(3, customer.getUserName());
			preparedStatement.setString(4, customer.getPassword());
			preparedStatement.setString(5, customer.getEmail());
			preparedStatement.setString(6, customer.getPhoneNumber());
			preparedStatement.setInt(7, customer.getCustomerId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to delete customer users from the customers table in the warehouse
	 * dataBase.
	 * 
	 * @param customer
	 *            which is an object modeled by Customer class.
	 */
	public void deleteCustomer(Customer customer) {
		String sql = "delete from customers where customer_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, customer.getCustomerId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
