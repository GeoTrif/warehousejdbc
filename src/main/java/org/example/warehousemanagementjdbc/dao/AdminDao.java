package org.example.warehousemanagementjdbc.dao;

import java.util.List;

import org.example.warehousemanagementjdbc.model.Admin;

/**
 * Interface used for admin data base operations implementation.
 * 
 * @author GeoTrif
 *
 */
public interface AdminDao {

	public void addAdmin(Admin admin);

	public Admin getAdminById(int adminId);

	public List<Admin> getAllAdmins();

	public void updateAdmin(Admin admin);

	public void deleteAdmin(Admin admin);

}
