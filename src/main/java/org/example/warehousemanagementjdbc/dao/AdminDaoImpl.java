package org.example.warehousemanagementjdbc.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.example.warehousemanagementjdbc.mapper.AdminMapper;
import org.example.warehousemanagementjdbc.model.Admin;
import org.example.warehousemanagementjdbc.util.ConnectionUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Operational class used to make operations on the admins table from the
 * warehouse dataBase.
 * 
 * @author GeoTrif
 *
 */
public class AdminDaoImpl implements AdminDao {
	private AdminMapper adminMapper = new AdminMapper();

	/**
	 * Used to add admin users to the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void addAdmin(Admin admin) {
		String sql = "insert into admins(first_name,last_name,user_name,password,email,phone_number) values (?,?,?,?,?,?)";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, admin.getFirstName());
			preparedStatement.setString(2, admin.getLastName());
			preparedStatement.setString(3, admin.getUserName());
			preparedStatement.setString(4, admin.getPassword());
			preparedStatement.setString(5, admin.getEmail());
			preparedStatement.setString(6, admin.getPhoneNumber());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to get admin users by admin id from the admins table in the warehouse
	 * dataBase.
	 * 
	 * @param adminId
	 *            which is an integer.
	 */
	public Admin getAdminById(int adminId) {
		String sql = "select * from admins where admin_id = ?";
		Admin admin = null;

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, adminId);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int id = resultSet.getInt("admin_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String userName = resultSet.getString("user_name");
				String password = resultSet.getString("password");
				String email = resultSet.getString("email");
				String phoneNumber = resultSet.getString("phone_number");

				System.out.println("Admin id: " + id);
				System.out.println("Admin First Name: " + firstName);
				System.out.println("Admin Last Name: " + lastName);
				System.out.println("Admin UserName: " + userName);
				System.out.println("Admin password: " + password);
				System.out.println("Admin email: " + email);
				System.out.println("Admin Phone Number: " + phoneNumber);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return admin;
	}

	/**
	 * Used to get all admins users from the admins table in the warehouse dataBase.
	 * 
	 */
	public List<Admin> getAllAdmins() {
		String sql = "select * from admins";
		List<Admin> admins = null;

		try {
			Statement statement = ConnectionUtil.makeConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int id = resultSet.getInt("admin_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String userName = resultSet.getString("user_name");
				String password = resultSet.getString("password");
				String email = resultSet.getString("email");
				String phoneNumber = resultSet.getString("phone_number");

				System.out.println("Admin id: " + id);
				System.out.println("Admin First Name: " + firstName);
				System.out.println("Admin Last Name: " + lastName);
				System.out.println("Admin UserName: " + userName);
				System.out.println("Admin password: " + password);
				System.out.println("Admin email: " + email);
				System.out.println("Admin Phone Number: " + phoneNumber);
			}

			admins = adminMapper.mapResultSetToAdmin(resultSet);

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return admins;
	}

	/**
	 * Used to update admin users from the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void updateAdmin(Admin admin) {
		String sql = "update admins set first_name = ?,last_name = ?,user_name = ?,password = ?,email = ?,phone_number = ? where admin_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setString(1, admin.getFirstName());
			preparedStatement.setString(2, admin.getLastName());
			preparedStatement.setString(3, admin.getUserName());
			preparedStatement.setString(4, admin.getPassword());
			preparedStatement.setString(5, admin.getEmail());
			preparedStatement.setString(6, admin.getPhoneNumber());
			preparedStatement.setInt(7, admin.getAdminId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to delete admin users from the admins table in the warehouse dataBase.
	 * 
	 * @param admin
	 *            which is an object modeled by Admin class.
	 */
	public void deleteAdmin(Admin admin) {
		String sql = "delete from admins where admin_id = ?";

		try {
			PreparedStatement preparedStatement = ConnectionUtil.makeConnection().prepareStatement(sql);
			preparedStatement.setInt(1, admin.getAdminId());
			preparedStatement.executeUpdate();
			preparedStatement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
