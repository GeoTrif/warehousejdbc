package org.example.warehousemanagementjdbc;

import java.io.IOException;

import org.example.warehousemanagementjdbc.controller.WarehouseController;

/**
 * Driver Class used to start the application.
 * 
 * @author GeoTrif
 *
 */
public class WarehouseApplication {

	/**
	 * Driver method.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		WarehouseController controller = new WarehouseController();
		controller.mainMenuFunctionality();
	}
}
